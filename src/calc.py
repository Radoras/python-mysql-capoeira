import datetime


def years_to_days(years):
    return years * 365


def calculate_past_date(days_from_now):
    return datetime.date.today() - datetime.timedelta(days=days_from_now)


def fetchall_single_column(cursor):
    return [x[0] for x in cursor.fetchall()]


def simple_remove_plural_from_snake_case(string):
    parts = string.split('_')
    return '_'.join(x[0:-1] if x[-1] == 's' else x for x in parts)


def snake_case_to_camel_case(string):
    parts = string.split('_')
    return ''.join(x.title() for x in parts)
