female_names = ['Anna', 'Molly', 'Elizabeth', 'Rebecca', 'Amber', 'Ella', 'Nicole', 'Martha', 'Hannah', 'Jasmine',
                'Amy', 'Sophie', 'Julia', 'Luna', 'Eliza', 'Emma', 'Sara', 'Gwen', 'Jessie', 'Vera', 'Karen',
                'Alice', 'Claudia', 'Lily', 'Alexa', 'Helena', 'Collins', 'Bethany', 'Natalie']

male_names = ['Jack', 'Olivier', 'Jacob', 'John', 'William', 'Richard', 'Edward', 'Luke', 'Thomas', 'Adam', 'David',
              'Daniel', 'Justin', 'Nolan', 'Pablo', 'Lukas', 'Hugh', 'Arthur', 'Roger', 'Ronald', 'Steve', 'Dustin',
              'Elliot', 'Nico', 'Clark', 'Eric', 'Andrew', 'Zak', 'Maximilian', 'Christopher', 'Ian', 'Bowen']

surnames = ['Evans', 'Parker', 'Roberts', 'Jackson', 'Jenkins', 'Smith', 'Wright', 'Carter', 'Bailet', 'Perry',
            'James', 'Rogers', 'Jones', 'Young', 'Barnes', 'Wood', 'Anderson', 'Williams', 'Brown', 'Reed',
            'Lee', 'Turner', 'King', 'Collins', 'Wilson', 'Lopez', 'Ford', 'Spencer', 'Webb', 'Moore']


countries = ["United States"]

cities = ["New York", "Los Angeles", "Chicago", "Houston", "Phoenix", "Philadelphia", "San Antonio", "San Diego",
          "Dallas", "San Jose", "Austin", "Jacksonville", "Fort Worth", "Columbus", "Charlotte", "San Francisco",
          "Indianapolis", "Seattle", "Denver", "Washington", "Boston", "El Paso", "Nashville", "Detroit"]

street_names = ["Chapet Street", "Oakwood Road", "St Andrew's Close", "Woodside", "St Paul's Road", "Home Close",
                "Mansfield Road", "Coronation Street", "Cambridge Street", "Grasmere Road", "Poplar Drive",
                "Waverley Road", "The Ridgeway", "The Croft", "Victoria Place", "Queensway", "Walnut Close",
                "Conway Close", "Oak Avenue", "Albion Street", "Derwent Close", "Wentworth Road", "Chestnut Grove"]
