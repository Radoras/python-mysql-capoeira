import getpass
from enum import Enum

import mysql.connector

import database_queries
from calc import *


def get_option(last_option_number):
    option = None
    while option not in range(1, last_option_number + 1):
        try:
            option = int(input("Select option: "))
        except Exception:
            print("Option must be an integer\n")
            option = None
    print()

    return option


def menu_insert_into(db, table_name):
    lower_name = table_name.lower()
    if lower_name not in database_queries.TABLES_NAMES:
        print("ERROR: WRONG TABLE NAME IN insert_into_menu python procedure")
        exit(1)

    print("INSERT INTO {}:".format(lower_name))
    column_names = database_queries.get_columns_names_from_table(db, lower_name)
    values = [input("{}: ".format(col_name)) for col_name in column_names]

    singular_camel_case_name = snake_case_to_camel_case(simple_remove_plural_from_snake_case(lower_name))
    eval('{}.{}(*values).insert_into_table_with_commit(db)'.format(lower_name, singular_camel_case_name))

    print("\nThe data has been inserted\n")


def main():
    password = getpass.getpass(prompt="Enter the password for the 'project_root' user: ")

    mydb = mysql.connector.connect(
        host='localhost',
        user='project_root',
        password=password,
        database='project'
    )

    refill_answer = 'y'
    if not database_queries.does_all_tables_exists(mydb) or input("Do you want to recreate database? [y/N] ") in ['Y', 'y']:
        database_queries.recreate_tables(mydb)
    else:
        refill_answer = input("Do you want to refill tables? [y/N] ")

    if refill_answer in ['y', 'Y']:
        database_queries.truncate_tables(mydb)
        database_queries.fill_all_tables_with_random_data(mydb)

    while True:
        print("Main Menu\n"
              "1. Get data\n"
              "2. Modify data\n"
              "3. Exit")
        option = get_option(last_option_number=3)

        if option == 1:
            while True:
                print("GET DATA\n"
                      "1.  Find instructors that did not create any event\n"
                      "2.  Find events created by instructors that created most events\n"
                      "3.  Find biggest events\n"
                      "4.  Find students without instructor\n"
                      "5.  Find members that was probably learning in given school\n"
                      "6.  Count degrees of members\n"
                      "7.  Find members with potential promotion\n"
                      "8.  Count members with potential promotion by degree\n"
                      "9.  [PLOT] Show degrees\n"
                      "10. [PLOT] Show members with potential promotion by degree\n"
                      "11. [PLOT] Show members due to possibile promotion\n"
                      "12. Get members that may be promoted and were at event\n"
                      "13. Back")
                option = get_option(last_option_number=13)

                result_df = None
                if option == 1:
                    print("Find instructors that did not create any event\n")
                    result_df = database_queries.find_instructors_that_did_not_create_any_event(mydb)

                elif option == 2:
                    print("Find events created by instructors that created most events\n")
                    result_df = database_queries.find_events_created_by_instructors_that_created_most_events(mydb)

                elif option == 3:
                    print("Find biggest events\n")
                    result_df = database_queries.find_biggest_events(mydb)

                elif option == 4:
                    print("Find students without instructor\n")
                    result_df = database_queries.find_students_without_instructor(mydb)
                elif option == 5:
                    print("Find members that was probably learning in given school\n")
                    school_id = input("Enter school id: ")
                    result_df = database_queries.find_members_that_was_probably_learning_in_given_school(mydb, school_id)

                elif option == 6:
                    print("Count degrees of members\n")
                    result_df = database_queries.count_degrees_of_members(mydb)

                elif option == 7:
                    print("Find members with potential promotion\n")
                    result_df = database_queries.find_members_with_potential_promotion(mydb)

                elif option == 8:
                    print("Count members with potential promotion by degree\n")
                    result_df = database_queries.count_members_with_potential_promotion_by_degree(mydb)

                elif option == 9:
                    print("[PLOT] Show degrees\n")
                    database_queries.show_degrees_plot(mydb)
                    continue

                elif option == 10:
                    print("[PLOT] Show members with potential promotion by degree\n")
                    database_queries.show_members_with_potential_promotion_by_degree_plot(mydb)
                    continue

                elif option == 11:
                    print("[PLOT] Show members due to possibile promotion\n")
                    database_queries.show_members_due_to_possibile_promotion_plot(mydb)
                    continue

                elif option == 12:
                    print("Get members that may be promoted and were at event\n")
                    event_id = input("Enter event id: ")
                    result_df = database_queries.get_members_that_may_be_promoted_and_were_at_event(mydb, event_id)

                else:
                    break

                print(result_df)
                print()
                input('Press Enter to continue')
                print()



        elif option == 2:
            while True:
                print("MODIFY DATA\n"
                      "1. Own INSERT INTO\n"
                      "2. Try to insert a member to closed school\n"
                      "3. Delete member\n"
                      "4. Delete random instructor\n"
                      "5. Delete instructor and move all its members to another one\n"
                      "6. Promote all members that may be updated and were on event\n"
                      "7. Close school and move all its members to another one\n"
                      "8. Back")
                option = get_option(last_option_number=8)

                if option == 1:
                    while True:
                        class options(Enum):
                            MEMBERS = 1
                            SCHOOLS = 2
                            SCHOOLS_MEMBERS = 3
                            EVENTS = 4
                            EVENTS_GUESTS = 5

                        print("OWN INSERT INTO\n"
                              "1. {}\n"
                              "2. {}\n"
                              "3. {}\n"
                              "4. {}\n"
                              "5. {}\n"
                              "6. Exit".format(*tuple(x.name.lower() for x in options)))
                        option = get_option(6)

                        if option == '6':
                            break
                        menu_insert_into(mydb, list(options.__members__.keys())[option - 1])

                elif option == 2:
                    print("Try to insert a member to closed school\n")
                    database_queries.try_to_insert_a_member_to_closed_school(mydb)

                elif option == 3:
                    print("Delete member\n")
                    member_id = input("Enter member id: ")
                    database_queries.delete_member(mydb, member_id)
                    print("Member with id={} has been removed".format(member_id))

                elif option == 4:
                    print("Delete random instructor\n")
                    deleted_rows = database_queries.delete_random_instructor(mydb)
                    print("{} record(s) deleted successfully".format(deleted_rows))
                    print("You may check now how many students does not have an instructor (1.4)")

                elif option == 5:
                    print("Delete instructor and move all its students to another one\n")
                    deleting_instructor_id = input("Enter deleting instructor id: ")
                    new_instructor_id = input("Enter instructor's id to which the students are to be assigned: ")
                    database_queries.delete_instructor_and_move_all_its_members_to_another_one(mydb, deleting_instructor_id, new_instructor_id)
                    print("Instructor with id={} has been deleted successfully".format(deleting_instructor_id))
                    print("Students of instructor {} have been assigned to instructor {}".format(deleting_instructor_id, new_instructor_id))

                elif option == 6:
                    print("Promote all members that may be updated and were on event\n")
                    event_id = input("Enter event id: ")
                    updated_records = database_queries.promote_all_members_that_may_be_updated_and_were_on_event(mydb, event_id)
                    print("{} record(s) have been updated".format(updated_records))

                elif option == 7:
                    print("Close school and move all its members to another one\n")
                    closing_school_id = input("Enter closing school id: ")
                    new_school_id = input("Enter shool's id to which the members are to be assigned: ")
                    database_queries.close_school_and_move_all_its_members_to_another_one(mydb, closing_school_id, new_school_id)
                    print("School with id={} has been closed successfully".format(closing_school_id))
                    print("Members from school {} have been assigned to school {}".format(closing_school_id, new_school_id))

                else:
                    break

                print()
                input('Press Enter to continue')
                print()

        else:
            break


if __name__ == '__main__':
    main()
