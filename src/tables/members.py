import math
import random

import numpy as np

import data
from calc import *

TABLE_NAME = 'members'
INSTRUCTOR_DEGREE = 6
DEGREES = 14


class Member:
    def __init__(self, first_name, last_name, sex, day_of_birth, start_date, degree, instructor_id):
        self.__first_name = first_name
        self.__last_name = last_name
        self.__sex = sex
        self.__day_of_birth = day_of_birth
        self.__start_date = start_date
        self.__degree = degree
        self.__instructor_id = instructor_id


    def insert_into_table_no_commit(self, db):
        mycursor = db.cursor()

        mycursor.execute("INSERT INTO " + TABLE_NAME + " (first_name, last_name, sex, day_of_birth, start_date, degree, instructor_id)"
                         "VALUES (%s, %s, %s, %s, %s, %s, %s)",
                         [self.__first_name,
                          self.__last_name,
                          self.__sex,
                          self.__day_of_birth,
                          self.__start_date,
                          self.__degree,
                          self.__instructor_id])


    def insert_into_table_with_commit(self, db):
        self.insert_into_table_no_commit(db)
        db.commit()


def create_table(db):
    mycursor = db.cursor()
    mycursor.execute("""
                     CREATE TABLE IF NOT EXISTS members (
                         id INT AUTO_INCREMENT PRIMARY KEY,
                         first_name VARCHAR(40) NOT NULL,
                         last_name VARCHAR(40) NOT NULL,
                         sex CHAR,
                         day_of_birth DATE NOT NULL,
                         start_date DATE NOT NULL,
                         degree TINYINT UNSIGNED NOT NULL DEFAULT 0,
                         instructor_id INT,
                         FOREIGN KEY (instructor_id) REFERENCES members(id) ON UPDATE CASCADE ON DELETE SET NULL,
                         CONSTRAINT members_max_degree_chk CHECK (degree <= 14),
                         CONSTRAINT members_dates_order_chk CHECK (day_of_birth <= start_date),
                         CONSTRAINT members_sex_value_chk CHECK (sex IN ('M', 'F', ''))
                     )""")

    mycursor.execute("DROP FUNCTION IF EXISTS does_member_exists")
    mycursor.execute("""
                     CREATE FUNCTION does_member_exists(id INT) RETURNS BOOLEAN
                     BEGIN
                        IF id IS NULL OR id IN (SELECT id FROM members) THEN
                            RETURN TRUE;
                        END IF;
                        RETURN FALSE;
                     END;""")

    mycursor.execute("DROP TRIGGER IF EXISTS check_if_instructor_exists_before_insert")
    mycursor.execute("""
                     CREATE TRIGGER check_if_instructor_exists_before_insert
                     BEFORE INSERT ON members
                     FOR EACH ROW BEGIN
                        IF NOT does_member_exists(NEW.instructor_id) THEN
                            SIGNAL SQLSTATE '45000'
                            SET MESSAGE_TEXT = "Cannot set member's instructor_id field to not existing member";
                        END IF;
                     END;""")

    mycursor.execute("DROP TRIGGER IF EXISTS check_if_instructor_exists_before_update")
    mycursor.execute("""
                     CREATE TRIGGER check_if_instructor_exists_before_update
                     BEFORE UPDATE ON members
                     FOR EACH ROW BEGIN
                        IF NOT does_member_exists(NEW.instructor_id) THEN
                            SIGNAL SQLSTATE '45000'
                            SET MESSAGE_TEXT = "Cannot set member's instructor_id field to not existing member";
                        END IF;
                     END;""")

    mycursor.execute("DROP PROCEDURE IF EXISTS set_to_null_if_empty")
    mycursor.execute("""
                     CREATE PROCEDURE set_to_null_if_empty(INOUT _input CHAR)
                     BEGIN
                        IF _input = '' THEN
                            SET _input = NULL;
                        END IF;
                     END;
                     """)

    mycursor.execute("DROP TRIGGER IF EXISTS set_member_sex_to_null_if_empty_before_insert")
    mycursor.execute("""
                     CREATE TRIGGER set_member_sex_to_null_if_empty_before_insert
                     BEFORE INSERT ON members
                     FOR EACH ROW BEGIN
                        CALL set_to_null_if_empty(NEW.sex);
                     END;""")

    mycursor.execute("DROP TRIGGER IF EXISTS set_member_sex_to_null_if_empty_before_update")
    mycursor.execute("""
                     CREATE TRIGGER set_member_sex_to_null_if_empty_before_update
                     BEFORE UPDATE ON members
                     FOR EACH ROW BEGIN
                        CALL set_to_null_if_empty(NEW.sex);
                     END;""")

    mycursor.execute("""DROP VIEW IF EXISTS instructors;""")
    mycursor.execute("CREATE VIEW instructors AS SELECT * FROM members WHERE degree >= %s", [INSTRUCTOR_DEGREE])


def get_study_years_range(degree):
    if degree == 0:
        return range(0, 1)
    elif degree == 1:
        return range(0, 2)
    elif degree == 2:
        return range(1, 3)
    elif degree == 3:
        return range(2, 5)
    elif degree == 4:
        return range(3, 7)
    elif degree == 5:
        return range(5, 8)
    elif degree == 6:
        return range(8, 14)
    elif degree == 7:
        return range(10, 18)
    elif degree == 8:
        return range(13, 23)
    elif degree == 9:
        return range(16, 29)
    elif degree == 10:
        return range(18, 33)
    elif degree == 11:
        return range(22, 36)
    elif degree == 12:
        return range(25, 41)
    elif degree == 13:
        return range(28, 46)
    elif degree == 14:
        return range(31, 55)


def calculate_reliable_studying_days(degree):
    study_years_range = get_study_years_range(degree)
    extra_start_days = 30 if degree == 0 else 0
    extra_end_days = 120 if degree == 0 else 0

    study_days_start_interval = years_to_days(study_years_range[0]) + extra_start_days
    study_years_end_interval = years_to_days(study_years_range[-1] + 1) + extra_end_days
    study_days = random.randint(study_days_start_interval, study_years_end_interval)

    return study_days


def random_members(degrees, instructors_id=None):
    earlies_start_days = years_to_days(15)

    sex_none_probability = 10
    sex_fill_probability = 100 - sex_none_probability
    members = list()

    for i in range(len(degrees)):
        sex = random.choice(('M', 'F'))
        inserted_sex = random.choices((sex, None), weights=[sex_fill_probability, sex_none_probability])[0]
        first_name = random.choice(data.female_names if sex == 'F' else data.male_names)
        surname = random.choice(data.surnames)

        degree = degrees[i]
        study_days = calculate_reliable_studying_days(degree)
        start_day = calculate_past_date(study_days)
        day_of_birth = start_day - datetime.timedelta(earlies_start_days + random.randint(0, years_to_days(20)))
        instructor_id = random.choice(instructors_id) if instructors_id else None

        members.append(Member(first_name, surname, inserted_sex, day_of_birth, start_day, degree, instructor_id))

    return members


def fill_member_table(db):

    def random_degrees():
        degrees = np.random.exponential(scale=2, size=random.randint(4000, 5500))
        degrees = [int(math.floor(x)) for x in degrees]
        degrees = [x for x in degrees if x in range(DEGREES + 1)]

        return degrees

    degrees = random_degrees()

    # create mestres and instructors
    instructors_degrees = [x for x in degrees if x in range(INSTRUCTOR_DEGREE, DEGREES + 1)]
    members = random_members(instructors_degrees)

    last_instructor_id = len(instructors_degrees)

    # create students
    members_degrees = [x for x in degrees if x in range(INSTRUCTOR_DEGREE)]
    members.extend(random_members(members_degrees, range(1, last_instructor_id)))

    for i in members:
        i.insert_into_table_no_commit(db)

    db.commit()

    print("{} table has been filled".format(TABLE_NAME))
