import random

import data
from calc import *

TABLE_NAME = 'schools'


class School:
    def __init__(self, country, city, postal_code, street_name, street_number, apartment_number, currently_open, close_date=None):
        self.__country = country
        self.__city = city
        self.__postal_code = postal_code
        self.__street_name = street_name
        self.__street_number = street_number
        self.__apartment_number = apartment_number
        self.__currently_open = currently_open
        self.__close_date = close_date


    def insert_into_table_no_commit(self, db):
        mycursor = db.cursor()

        mycursor.execute("INSERT INTO " + TABLE_NAME + " (country, city, postal_code, street_name, street_number, apartment_number, currently_open, close_date)"
                         "VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
                         [self.__country,
                          self.__city,
                          self.__postal_code,
                          self.__street_name,
                          self.__street_number,
                          self.__apartment_number,
                          self.__currently_open,
                          self.__close_date])


    def insert_into_table_with_commit(self, db):
        self.insert_into_table_no_commit(db)
        db.commit()


def create_table(db):
    mycursor = db.cursor()
    mycursor.execute("""
                     CREATE TABLE IF NOT EXISTS schools (
                         id INT AUTO_INCREMENT PRIMARY KEY,
                         country VARCHAR(30) NOT NULL,
                         city VARCHAR(30) NOT NULL,
                         postal_code CHAR(5) NOT NULL,
                         street_name VARCHAR(40) NOT NULL,
                         street_number SMALLINT UNSIGNED NOT NULL,
                         apartment_number SMALLINT UNSIGNED,
                         currently_open BOOLEAN NOT NULL DEFAULT TRUE,
                         close_date DATE,
                         CONSTRAINT postal_code_chk CHECK (postal_code NOT LIKE '[0-9]{5}')
                     )""")

    mycursor.execute("DROP PROCEDURE IF EXISTS set_close_date")
    mycursor.execute("""
                     CREATE PROCEDURE set_close_date(IN _close_date DATE, OUT _open_flag BOOLEAN)
                     BEGIN
                        IF _close_date IS NULL THEN
                            SET _open_flag = TRUE;
                        ELSE
                            SET _open_flag = FALSE;
                        END IF;
                     END;""")

    mycursor.execute("DROP TRIGGER IF EXISTS schools_set_close_date_before_insert")
    mycursor.execute("""
                     CREATE TRIGGER schools_set_close_date_before_insert
                     BEFORE INSERT ON schools
                     FOR EACH ROW BEGIN
                        CALL set_close_date(NEW.close_date, NEW.currently_open);
                     END;""")

    mycursor.execute("DROP TRIGGER IF EXISTS schools_set_close_date_before_update")
    mycursor.execute("""
                     CREATE TRIGGER schools_set_close_date_before_update
                     BEFORE UPDATE ON schools
                     FOR EACH ROW BEGIN
                        CALL set_close_date(NEW.close_date, NEW.currently_open);
                     END;""")


def random_school():
    max_street_number = 500
    max_apartment_number = 100

    country = random.choice(data.countries)
    city = random.choice(data.cities)
    postal_code = ''.join(random.choice('0123456789') for _ in range(5))
    street_name = random.choice(data.street_names)
    street_number = random.randint(1, max_street_number)
    apartment_number = random.randint(1, max_apartment_number)
    currently_open = random.choices([True, False], weights=[80, 20])[0]

    past_days_for_close_date = random.randrange(0, years_to_days(40))
    close_date = None if currently_open else calculate_past_date(past_days_for_close_date)

    school = School(country, city, postal_code, street_name, street_number, apartment_number, currently_open, close_date)

    return school


def fill_schools_table(db):
    min_schools = 70
    max_schools = 115

    for _ in range(0, random.randint(min_schools, max_schools)):
        random_school().insert_into_table_no_commit(db)

    db.commit()

    print("{} table has been filled".format(TABLE_NAME))
