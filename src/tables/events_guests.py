import random

import numpy as np

from calc import *

TABLE_NAME = 'events_guests'


class EventGuest:
    def __init__(self, event_id, member_id):
        self.__event_id = event_id
        self.__member_id = member_id


    def insert_into_table_no_commit(self, db):
        mycursor = db.cursor()

        mycursor.execute("INSERT INTO " + TABLE_NAME + " VALUES (%s, %s)", [self.__event_id, self.__member_id])


    def insert_into_table_with_commit(self, db):
        self.insert_into_table_no_commit(db)
        db.commit()


def create_table(db):
    mycursor = db.cursor()
    mycursor.execute("""
                     CREATE TABLE IF NOT EXISTS events_guests (
                         event_id INT NOT NULL,
                         member_id INT NOT NULL,
                         PRIMARY KEY (event_id, member_id),
                         FOREIGN KEY (event_id) REFERENCES events(id) ON UPDATE CASCADE ON DELETE CASCADE,
                         FOREIGN KEY (member_id) REFERENCES members(id) ON UPDATE CASCADE ON DELETE CASCADE
                     )""")


def fill_events_guests_table(db):
    mycursor = db.cursor(buffered=True)

    mycursor.execute("SELECT id FROM members")
    members_id = fetchall_single_column(mycursor)

    mycursor.execute("SELECT id FROM events")
    events_id = fetchall_single_column(mycursor)

    events_guests = list()
    for event_id in events_id:
        mycursor.execute("SELECT organizer_id FROM events WHERE id = %s", [event_id])
        organizer_id = mycursor.fetchone()[0]
        organizer_list_id = organizer_id - 1    # MySQL starts with 1, Python with 0

        potential_guests_list = members_id[0:organizer_list_id] + members_id[organizer_list_id+1:-1]
        random_guest_data = np.random.exponential(scale=100, size=1)[0] - np.random.exponential(scale=1.75, size=1)[0]
        guests_number = int(20 + round(random_guest_data))
        if guests_number <= 0:
            guests_number = random.randint(1, 5)
        random_guests = random.sample(potential_guests_list, k=guests_number)

        events_guests.append(EventGuest(event_id, organizer_id))
        events_guests.extend(EventGuest(event_id, guest_id) for guest_id in random_guests)

    for eg in events_guests:
        eg.insert_into_table_no_commit(db)

    db.commit()

    print("{} table has been filled".format(TABLE_NAME))
