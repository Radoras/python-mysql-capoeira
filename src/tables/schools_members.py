import math
import random

from calc import *

TABLE_NAME = 'schools_members'


class SchoolMember:
    def __init__(self, member_id, school_id, start_date, end_date):
        self.__member_id = member_id
        self.__school_id = school_id
        self.__start_date = start_date
        self.__end_date = end_date


    def insert_into_table_no_commit(self, db):
        mycursor = db.cursor()

        mycursor.execute("INSERT INTO " + TABLE_NAME + " (member_id, school_id, start_date, end_date)"
                         "VALUES (%s, %s, %s, %s, %s, %s, %s)",
                         [self.__member_id, self.__school_id, self.__start_date, self.__end_date])


    def insert_into_table_with_commit(self, db):
        self.insert_into_table_no_commit(db)
        db.commit()


def create_table(db):
    mycursor = db.cursor()

    mycursor.execute("""
                     CREATE TABLE IF NOT EXISTS schools_members (
                         member_id INT NOT NULL,
                         school_id INT NOT NULL,
                         start_date DATE NOT NULL,
                         end_date DATE,
                         PRIMARY KEY (member_id, school_id, start_date),
                         FOREIGN KEY (member_id) REFERENCES members(id) ON UPDATE CASCADE ON DELETE CASCADE,
                         FOREIGN KEY (school_id) REFERENCES schools(id) ON UPDATE CASCADE ON DELETE NO ACTION
                     )""")
    mycursor.execute("""
                     ALTER TABLE schools_members
                     ADD CONSTRAINT schools_members_dates_order_chk CHECK (start_date <= end_date)""")

    mycursor.execute("DROP FUNCTION IF EXISTS check_if_school_was_open")
    mycursor.execute("SET GLOBAL log_bin_trust_function_creators = 1")
    mycursor.execute("""
                     CREATE FUNCTION check_if_school_was_open (_school_id INT, _date DATE)
                     RETURNS BOOLEAN
                     BEGIN
                        DECLARE _open BOOLEAN;
                        DECLARE _close_date DATE;
                        
                        SELECT currently_open INTO _open FROM schools WHERE id=_school_id;
                        SELECT close_date INTO _close_date FROM schools WHERE id=_school_id;
                        
                        IF _open = TRUE OR (_date != NULL AND DATE(_date) < DATE(_close_date)) THEN
                            RETURN TRUE;
                        ELSE
                            RETURN FALSE;
                        END IF;
                     END;
                     """)

    # For some reasons Python does not allows to create multi=True when TRIGGERS are creating
    mycursor.execute("DROP TRIGGER IF EXISTS members_schools_insert_chk")
    mycursor.execute("""
                     CREATE TRIGGER members_schools_insert_chk
                     BEFORE INSERT ON schools_members
                     FOR EACH ROW BEGIN
                         IF NOT check_if_school_was_open(NEW.school_id, NEW.end_date) THEN
                             SIGNAL SQLSTATE '45000'
                             SET MESSAGE_TEXT = "Cannot insert a member into a closed school";
                         END IF;
                     END;""")

    db.commit()


def random_school(db, schools_ids, date):
    mycursor = db.cursor()

    while True:
        school_id = random.choice(schools_ids)
        mycursor.execute("SELECT close_date FROM schools WHERE id=%s", [school_id])
        school_close_date = mycursor.fetchone()[0]

        if not school_close_date or school_close_date > date:
            return school_id


def fill_schools_members_table(db):
    mycursor = db.cursor()

    mycursor.execute("SELECT id FROM schools WHERE currently_open = 1")
    schools_ids = fetchall_single_column(mycursor)

    # members_schools
    mycursor.execute("SELECT id FROM members")
    members_ids = [x[0] for x in mycursor.fetchall() if x[0]]

    for member_id in members_ids:
        mycursor.execute("SELECT start_date, DATEDIFF(CURDATE(), start_date) 'capoeira_days', degree FROM members WHERE id = %s", [member_id])
        start_date, capoeira_days, degree = mycursor.fetchone()
        transfers_between_schools = random.choices(range(degree + 1), weights=[math.sqrt(degree + 1 - x) for x in range(degree + 1)])[0]

        schools_date = [start_date]
        member_schools_list = list()

        if transfers_between_schools > 0 and capoeira_days > 5:
            days_left = capoeira_days
            schools_left = transfers_between_schools
            for _ in range(transfers_between_schools):
                days_in_school = random.randrange(math.ceil(days_left / (schools_left + days_left / 20)), math.ceil(days_left / schools_left))
                school_end_date = schools_date[-1] + datetime.timedelta(days=days_in_school)
                schools_date.append(school_end_date)

                school_id = random_school(db, schools_ids, school_end_date)
                member_schools_list.append(school_id)

                days_left -= days_in_school
                schools_left -= 1

        member_schools_list.append(random_school(db, schools_ids, datetime.date.today()))

        schools_date.append(None)     # last (not finished yet) school

        mycursor.executemany("INSERT INTO schools_members (member_id, school_id, start_date, end_date) VALUES (%s, %s, %s, %s)",
                             [[member_id, member_schools_list[i], schools_date[i], schools_date[i+1]]
                              for i in range(len(schools_date[0:-1]))])

    db.commit()

    print("{} table has been filled".format(TABLE_NAME))
