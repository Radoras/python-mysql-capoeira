import random

from calc import *
from tables import members

TABLE_NAME = 'events'


class Event:
    def __init__(self, start_date, end_date, organizer_id, school_id):
        self.__start_date = start_date
        self.__end_date = end_date
        self.__organizer_id = organizer_id
        self.__school_id = school_id


    def insert_into_table_no_commit(self, db):
        mycursor = db.cursor()

        mycursor.execute("INSERT INTO " + TABLE_NAME + " (start_date, end_date, organizer_id, school_id) VALUES (%s, %s, %s, %s)",
                         [self.__start_date, self.__end_date, self.__organizer_id, self.__school_id])


    def insert_into_table_with_commit(self, db):
        self.insert_into_table_no_commit(db)
        db.commit()


def create_table(db):
    mycursor = db.cursor()
    mycursor.execute("""
                     CREATE TABLE IF NOT EXISTS events (
                         id INT AUTO_INCREMENT PRIMARY KEY,
                         start_date DATE NOT NULL,
                         end_date DATE NOT NULL,
                         organizer_id INT,
                         school_id INT NOT NULL,
                         FOREIGN KEY (organizer_id) REFERENCES members(id) ON UPDATE CASCADE ON DELETE SET NULL,
                         FOREIGN KEY (school_id) REFERENCES schools(id),
                         CONSTRAINT events_dates_order_chk CHECK (start_date <= end_date)
                     )""")


def find_potential_organizers_ids(db):
    mycursor = db.cursor()
    mycursor.execute("SELECT id FROM members WHERE degree >= %s", [members.INSTRUCTOR_DEGREE])
    members_ids = fetchall_single_column(mycursor)
    return members_ids


def calculate_reliable_event_date_and_school(db, organizer_id):
    mycursor = db.cursor(buffered=True)

    instructor_min_years = members.get_study_years_range(members.INSTRUCTOR_DEGREE)[0]
    mycursor.execute("""
                     SELECT DATEDIFF(CURDATE(), DATE_ADD(start_date, INTERVAL %s YEAR)) 'Days as instructor'
                     FROM members
                     WHERE id=%s""",
                     [instructor_min_years, organizer_id])

    max_days_as_instructor = mycursor.fetchone()[0]     # only instructor can create events
    days_from_now_when_event_happened = random.randrange(max_days_as_instructor) if max_days_as_instructor > 0 else 0
    event_date = calculate_past_date(days_from_now_when_event_happened)

    mycursor.execute("""
                     SELECT school_id
                     FROM schools_members
                     WHERE member_id = %s AND %s BETWEEN start_date AND IFNULL(end_date, CURDATE())""",
                     [organizer_id, event_date])
    where_organizer_taught = mycursor.fetchone()[0]

    return event_date, where_organizer_taught


def fill_events_table(db):
    events_number = random.randint(500, 1000)
    organizers_list = find_potential_organizers_ids(db)

    events = list()
    for i in range(events_number):
        days = random.randint(0, 14)
        organizer_id = random.choice(organizers_list)
        start_date, school_id = calculate_reliable_event_date_and_school(db, organizer_id)
        end_date = start_date + datetime.timedelta(days=days)

        events.append(Event(start_date, end_date, organizer_id, school_id))

    for ev in events:
        ev.insert_into_table_no_commit(db)

    db.commit()

    print("{} table has been filled".format(TABLE_NAME))
