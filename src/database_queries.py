import matplotlib.pyplot as plt
import mysql
import pandas as pd

from tables import members, schools, events, events_guests, schools_members

TABLES_NAMES = ['members', 'instructors', 'schools', 'schools_members', 'events', 'events_guests']


def create_tables(db):
    members.create_table(db)
    schools.create_table(db)
    events.create_table(db)
    events_guests.create_table(db)
    schools_members.create_table(db)


def drop_tables(db):
    mycursor = db.cursor()

    mycursor.execute("SET FOREIGN_KEY_CHECKS = 0")
    mycursor.execute("DROP TABLE IF EXISTS members")
    mycursor.execute("DROP TABLE IF EXISTS schools")
    mycursor.execute("DROP TABLE IF EXISTS schools_members")
    mycursor.execute("DROP TABLE IF EXISTS events")
    mycursor.execute("DROP TABLE IF EXISTS events_guests")
    mycursor.execute("SET FOREIGN_KEY_CHECKS = 1")


def truncate_tables(db):
    mycursor = db.cursor()

    mycursor.execute("SET FOREIGN_KEY_CHECKS = 0")
    mycursor.execute("TRUNCATE TABLE members")
    mycursor.execute("TRUNCATE TABLE schools")
    mycursor.execute("TRUNCATE TABLE schools_members")
    mycursor.execute("TRUNCATE TABLE events")
    mycursor.execute("TRUNCATE TABLE events_guests")
    mycursor.execute("SET FOREIGN_KEY_CHECKS = 1")


def recreate_tables(db):
    drop_tables(db)
    create_tables(db)


def does_all_tables_exists(db):
    mycursor = db.cursor()

    mycursor.execute("SHOW TABLES")
    tables = mycursor.fetchall()
    if len(tables) != len(TABLES_NAMES):
        return False

    tables_names = [tables[i][0] for i in range(len(TABLES_NAMES))]
    return sorted(tables_names) == sorted(TABLES_NAMES)


def fill_all_tables_with_random_data(db):
    members.fill_member_table(db)
    schools.fill_schools_table(db)
    schools_members.fill_schools_members_table(db)
    events.fill_events_table(db)
    events_guests.fill_events_guests_table(db)
    print()


# -------------------------------------------------------------------------

def get_columns_names_from_table(db, table_name):
    mycursor = db.cursor()
    mycursor.execute("DESCRIBE " + table_name)
    description = mycursor.fetchall()

    columns = [col[0] for col in description]
    if columns[0] == 'id':
        columns = columns[1:]

    return columns


def find_instructors_that_did_not_create_any_event(db):
    # IN - may be more than 1 instructor that created most events
    results_df = pd.read_sql("""
                             SELECT * FROM events WHERE organizer_id IN
                             (SELECT organizer_id  FROM events GROUP BY organizer_id HAVING COUNT(*)=0)
                             ORDER BY organizer_id""", con=db)
    return results_df


def find_events_created_by_instructors_that_created_most_events(db):
    # IN - may be more than 1 instructor that created most events
    results_df = pd.read_sql("""
                             SELECT * FROM events WHERE organizer_id IN
                             (SELECT organizer_id  FROM events GROUP BY organizer_id HAVING COUNT(*) =
                             (SELECT COUNT(*) as c FROM events GROUP BY organizer_id ORDER BY c DESC LIMIT 1))
                             ORDER BY organizer_id""", con=db)
    return results_df


def find_biggest_events(db):
    results_df = pd.read_sql("""
                             SELECT e.*, COUNT(*) AS 'guests'
                             FROM events AS e INNER JOIN events_guests AS eg ON e.id = eg.event_id WHERE eg.event_id IN 
                             (SELECT event_id FROM events_guests GROUP BY event_id HAVING COUNT(*) =
                             (SELECT COUNT(*) as c FROM events_guests GROUP BY event_id ORDER BY c DESC LIMIT 1))
                             GROUP BY e.id ORDER BY e.start_date""", con=db)
    return results_df


def try_to_insert_a_member_to_closed_school(db):
    mycursor = db.cursor()

    mycursor.execute("SELECT id FROM schools WHERE currently_open = FALSE LIMIT 1")
    closed_school_id = mycursor.fetchone()[0]

    mycursor.execute("SELECT id FROM members LIMIT 1")
    existing_member_id = mycursor.fetchone()[0]

    try:
        mycursor.execute("INSERT INTO schools_members (member_id, school_id, start_date) VALUES (%s, %s, CURDATE())",
                         [existing_member_id, closed_school_id])
    except mysql.connector.Error:
        print("ERROR: Cannot insert a member into a closed school")

    db.commit()


def delete_member(db, member_id):
    mycursor = db.cursor()
    mycursor.execute("DELETE FROM members WHERE id=%s", [member_id])
    db.commit()


def delete_random_instructor(db):
    mycursor = db.cursor()
    mycursor.execute("""
                     DELETE FROM instructors
                     WHERE id IN
                        (SELECT * FROM
                            (SELECT id FROM instructors ORDER BY RAND() LIMIT 1) random_instructor
                        )""")
    db.commit()

    return mycursor.rowcount


def find_students_without_instructor(db):
    results_df = pd.read_sql("SELECT id FROM members WHERE instructor_id IS NULL AND degree < %s",
                             params=[members.INSTRUCTOR_DEGREE], con=db)
    return results_df


def find_members_that_was_probably_learning_in_given_school(db, school_id):
    """
    :param db:
    :param school_id:
    :return: get members that was students when they were in specific school
    """
    longest_time_as_student = members.get_study_years_range(members.INSTRUCTOR_DEGREE - 1)[-1] + 1
    results_df = pd.read_sql("""
                             SELECT DISTINCT m.id FROM members m
                             INNER JOIN schools_members sm ON m.id = sm.member_id
                             WHERE sm.school_id = %s
                             AND TIMESTAMPDIFF(YEAR, m.start_date, sm.start_date) <= %s
                             """, params=[school_id, longest_time_as_student], con=db)
    return results_df


def count_degrees_of_members(db):
    results_df = pd.read_sql("SELECT degree, COUNT(*) AS counter FROM members GROUP BY degree ORDER BY degree", con=db)
    return results_df


def find_members_with_potential_promotion(db):
    results_df = pd.read_sql("""
                             SELECT id FROM members
                             WHERE TIMESTAMPDIFF(YEAR, CURDATE(), start_date) >=
                             ELT(degree + 1, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""",
                             params=[members.get_study_years_range(degree + 1)[0] for degree in range(members.DEGREES)], con=db)
    return results_df


def count_members_with_potential_promotion_by_degree(db):
    results_df = pd.read_sql("""
                             SELECT degree, COUNT(*) AS counter FROM members
                             WHERE TIMESTAMPDIFF(YEAR, start_date, CURDATE()) >=
                             ELT(degree + 1, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
                             GROUP BY degree
                             ORDER BY degree""",
                             params=[members.get_study_years_range(degree + 1)[0] for degree in range(members.DEGREES)],
                             con=db)
    return results_df


def show_degrees_plot(db):
    degrees_df = count_degrees_of_members(db)
    degrees_df.plot(x='degree', y='counter', kind='bar', legend=False)
    for x, counter in enumerate(degrees_df.counter):
        plt.annotate(str(counter), (x, counter), textcoords='offset points', xytext=(0,2), ha='center')
    plt.show()


def show_members_with_potential_promotion_by_degree_plot(db):
    promotion_by_degree_df = count_members_with_potential_promotion_by_degree(db)
    promotion_by_degree_df.plot(x='degree', y='counter', kind='bar', legend=False)
    for x, counter in enumerate(promotion_by_degree_df.counter):
        plt.annotate(str(counter), (x, counter), textcoords='offset points', xytext=(0,2), ha='center')
    plt.show()


def show_members_due_to_possibile_promotion_plot(db):
    df = pd.read_sql("""
                     SELECT degree, if(possible_promotion, 'YES', 'NO') promotion
                     FROM (
                        SELECT *, TIMESTAMPDIFF(YEAR, start_date, CURDATE()) >=
                        ELT(degree + 1, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
                        AS possible_promotion FROM members
                     ) AS promoting_members
                     ORDER BY degree""",
                     params=[members.get_study_years_range(degree + 1)[0] for degree in range(members.DEGREES)],
                     con=db)

    df.groupby(['degree', 'promotion']).size().unstack().plot(kind='bar', stacked=True, color=['red', 'blue'])

    members_degrees = count_degrees_of_members(db).counter.tolist()
    members_degrees.extend([0] * ((members.DEGREES + 1) - len(members_degrees)))

    potential_promotion_members_degrees = count_members_with_potential_promotion_by_degree(db).counter.tolist()
    potential_promotion_members_degrees.extend([0] * ((members.DEGREES + 1) - len(potential_promotion_members_degrees)))

    no_promotion_members_degrees = [x1 - x2 for (x1, x2) in zip(members_degrees, potential_promotion_members_degrees)]

    for x, counter in enumerate(no_promotion_members_degrees):
        plt.annotate(str(counter), (x, members_degrees[x]), textcoords='offset points', xytext=(0, 0), ha='right', color='red')

    for x, counter in enumerate(potential_promotion_members_degrees):
        plt.annotate(str(counter), (x, members_degrees[x]), textcoords='offset points', xytext=(0, 0), ha='left', color='blue')

    plt.show()


def get_members_that_may_be_promoted_and_were_at_event(db, event_id):
    needed_years_by_range = [members.get_study_years_range(degree + 1)[0] for degree in range(members.DEGREES)]
    params = [event_id]
    params.extend(needed_years_by_range)

    results_df = pd.read_sql("""
                             SELECT id FROM members m INNER JOIN events_guests eg ON m.id=eg.member_id
                             WHERE eg.event_id = %s
                             AND TIMESTAMPDIFF(YEAR, m.start_date, CURDATE()) >=
                             ELT(degree + 1, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""",
                             params=params,
                             con=db)
    return results_df


def promote_all_members_that_may_be_updated_and_were_on_event(db, event_id):
    mycursor = db.cursor()

    needed_years_by_range = [members.get_study_years_range(degree + 1)[0] for degree in range(members.DEGREES)]
    params = [event_id]
    params.extend(needed_years_by_range)

    mycursor.execute("""
                     UPDATE members m INNER JOIN events_guests eg ON m.id=eg.member_id
                     SET degree = degree + 1
                     WHERE eg.event_id = %s
                     AND TIMESTAMPDIFF(YEAR, m.start_date, CURDATE()) >=
                     ELT(degree + 1, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""",
                     params=params)
    db.commit()

    return mycursor.rowcount


def close_school_and_move_all_its_members_to_another_one(db, school_id, new_school_id):
    mycursor = db.cursor()

    mycursor.execute("SELECT currently_open FROM schools WHERE id=%s", [new_school_id])
    is_new_school_open = mycursor.fetchone()[0]

    if is_new_school_open is None:
        raise Exception('Wrong new school id')

    if not is_new_school_open:
        raise Exception('The school with id={} is closed.\n'
                        'The school with id={} has not been closed and their members have not been moved.'
                        .format(school_id, new_school_id))

    mycursor.execute("SELECT * FROM schools WHERE id=%s", [school_id])
    old_school_data = mycursor.fetchone()[0]

    if old_school_data is None:
        raise Exception('The old school does not exists')

    mycursor.execute("UPDATE schools SET currently_open=FALSE, close_date=CURDATE() WHERE id=%s", [school_id])
    mycursor.execute("UPDATE schools_members SET school_id=%s WHERE school_id=%s", [new_school_id, school_id])

    db.commit()


def delete_instructor_and_move_all_its_members_to_another_one(db, instructor_id, new_instructor_id):
    mycursor = db.cursor()

    mycursor.execute("UPDATE members SET instructor_id=%s WHERE instructor_id=%s", [new_instructor_id, instructor_id])
    mycursor.execute("DELETE FROM members WHERE id=%s", [instructor_id])

    db.commit()
